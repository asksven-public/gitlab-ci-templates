# gitlab-ci-templates

A gitlab-ci template collection.

## Usage

In your `.gitlab.ci.yml`

```yaml
include:
  - project: 'asksven-public/gitlab-ci-templates'
    ref: master
    file: '.gitlab-ci-template.yml'
```